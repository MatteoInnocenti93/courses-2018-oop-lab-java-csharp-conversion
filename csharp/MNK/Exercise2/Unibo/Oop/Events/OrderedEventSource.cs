﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    class OrderedEventSource<T> : AbstractEventSource<T>
    {
        private IList<EventListener<T>> eventListeners = new List<EventListener<T>>();

        protected override ICollection<EventListener<T>> getEventListeners()
        {
            return eventListeners;
        }
    }
}
