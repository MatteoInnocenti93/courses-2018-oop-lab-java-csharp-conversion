﻿using System;
namespace Unibo.Oop.Events
{
    public interface IEventEmitter<TArg>
    {
        void Emit(TArg data);

        IEventSource<TArg> EventSource { get; }
    }
}
