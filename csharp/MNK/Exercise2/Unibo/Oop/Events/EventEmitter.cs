﻿using System;
namespace Unibo.Oop.Events
{
    public static class EventEmitter
    {
        public static IEventEmitter<TArg> Ordered<TArg>() 
        {
            return new OrderedEventSource<TArg>();
        }

        public static IEventEmitter<TArg> Unordered<TArg>()
        {
            return new UnorderedEventSource<TArg>();
        }
    }
}
