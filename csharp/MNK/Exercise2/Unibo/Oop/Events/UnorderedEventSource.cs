﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    class UnorderedEventSource<T> : AbstractEventSource<T>
    {
        private HashSet<EventListener<T>> eventListeners = new HashSet<EventListener<T>>();
        protected override ICollection<EventListener<T>> getEventListeners()
        {
            return eventListeners;
        }
    }
}
