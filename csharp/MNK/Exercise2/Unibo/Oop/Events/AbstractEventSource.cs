﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    abstract class AbstractEventSource<T> : IEventEmitter<T>, IEventSource<T>
    {
        protected abstract ICollection<EventListener<T>> getEventListeners();

        public IEventSource<T> EventSource => this;
        public IEventEmitter<T> EventEmitter => this;
       
        public void Bind(EventListener<T> eventListener)
        {
            getEventListeners().Add(eventListener);
        }

        public void Unbind(EventListener<T> eventListener)
        {
            getEventListeners().Remove(eventListener);
        }

        public void UnbindAll()
        {
            getEventListeners().Clear();
        }

        public void Emit(T data)
        {
            foreach(var elem in getEventListeners())
            {
                elem.Invoke(data);
            }
        }
    }
}
