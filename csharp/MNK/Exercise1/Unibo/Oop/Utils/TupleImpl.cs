﻿using System;
using System.Linq;

namespace Unibo.Oop.Utils
{
    internal class TupleImpl : ITuple
    {
        public readonly Object[] items;

        public TupleImpl(object[] args)
        {
            this.items = args;
        }

        public int Length => this.items.Length;

        public object this[int i] => this.items[i];

        public object[] ToArray()
        {
            return (Object[])this.items.Clone();
        }

        public override String ToString()
        {
            string finalString = "";
            if (this.items.Length > 0)
            {
                finalString += "(";
                foreach (var elem in this.items)
                {
                    finalString += (elem.ToString());
                    if(!elem.Equals(this.items.Last()))
                    {
                        finalString += ", ";
                    }
                }
                finalString += ")";
            }
            return finalString;
        }

        public override int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            foreach(var elem in this.items)
            {
                result = prime ^ result + elem.GetHashCode();
            }
            return result;
        }

        public override bool Equals(Object obj)
        {
            return obj != null
                   && obj is TupleImpl
                   && (((TupleImpl)obj).items).SequenceEqual(this.items);
        }
    }
}
